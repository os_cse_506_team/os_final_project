#include <stdio.h>
#include <sys/kprintf.h>
#include <sys/defs.h>
#include <sys/struct_utility.h>


void contextSwitch(struct task_struct *pcb1, struct task_struct *pcb2);

struct task_struct pcb1;
struct task_struct pcb2;
void thread1();
void thread2();


void preparePCB2(){
	/*for(int i = 4000; i < 4059; i++){
		pcb2.kstack[i] = 0;
	}
	//pcb2.kstack[4066] = (uint64_t) &thread2;
	pcb2.kstack[4067] = (uint64_t) &thread2 >> 56;
	pcb2.kstack[4066] = (uint64_t) &thread2 >> 48;
	pcb2.kstack[4065] = (uint64_t) &thread2 >> 40;
        pcb2.kstack[4064] = (uint64_t) &thread2 >> 32;
	pcb2.kstack[4063] = (uint64_t) &thread2 >> 24;
        pcb2.kstack[4062] = (uint64_t) &thread2 >> 16;
	pcb2.kstack[4061] = (uint64_t) &thread2 >> 8;
        pcb2.kstack[4060] = (uint64_t) &thread2;
	pcb2.rsp = (uint64_t) &pcb2.kstack[4000];*/

	for(int i = 450; i < 465; i++){
                pcb2.kstack[i] = 0;
        }
        //pcb2.kstack[4066] = (uint64_t) &thread2;
        pcb2.kstack[465] = (uint64_t) &thread2;
        pcb2.rsp = (uint64_t) &pcb2.kstack[450];
}

void thread1(){
	preparePCB2();	
	int i = 0;
	while(i<4){
		kprintf("value of i is %d \n", i);
		i++;
		contextSwitch(&pcb1, &pcb2);
	}
}

void thread2(){
	int j = 0;
	while(j<4){
		kprintf("value of j is %d \n", j);
		j++;
		contextSwitch(&pcb2, &pcb1);
	}
}

/*void contextSwitch(struct task_struct *pcb1, struct task_struct *pcb2){
	__asm__ __volatile__(
		"pushq %%rax;"
		"pushq %%rbx;"
		"pushq %%rcx;"
		"pushq %%rdx;"
		"pushq %%rsi;"
		"pushq %%rdi;"
		"pushq %%rbp;"
		"pushq %%rsp;"
		"pushq %%r8;"
		"pushq %%r9;"
		"pushq %%r10;"
                "pushq %%r11;"
                "pushq %%r12;"
                "pushq %%r13;"
                "pushq %%r14;"
                "pushq %%r15;"
		"movq %%rsp %0;"
		"movq %1 %%rsp;"
		"popq %%r15;"
		"popq %%r14;"
		"popq %%r13;"
                "popq %%r12;"
		"popq %%r11;"
                "popq %%r10;"
		"popq %%r9;"
                "popq %%r8;"
		"popq %%rsp;"
                "popq %%rbp;"
		"popq %%rdi;"
                "popq %%rsi;"
		"popq %%rdx;"
                "popq %%rcx;"
		"popq %%rbx;"
                "popq %%rax;"
		"retq;"
		: "=a" (pcb1)
		: "r" (pcb2)
		: "rax", "rbx", "rcx", "rdx", "rsi" ,"rdi", "rbp", "rsp", "r8", "r9", "r10", "r11", "r12", "r13", "r14", "r15"
	);*/

	
/*		__asm__ __volatile__ (
                "movq %0, %%rax;"
                "movq %%rax, %%cr3;"
                "movq %%cr0, %%rax;"
                "or $0x80000001, %%eax;"
                "movq %%rax, %%cr0;"
                :
                : "r" (plm_address)
                : "rax");*/
	
//}

/*void thread1(){
	struct task_struct pcb1 = {.pid = 0};
	if(pcb1.pid == 0){}
	struct task_struct pcb2 = {.pid = 1};
        if(pcb2.pid == 1){}
        uint64_t i = 0;
	while(i < 2){
        	i = i + 1;
        	kprintf("value of i %d \n", i);
		context_switch(&pcb1, &pcb2);
	}
}

void context_switch(struct task_struct *pcb1, struct task_struct *pcb2){
	//Load all register values in 
	__asm__ __volatile__ (
		"movq %%rax, "				
		:
		:
		:
	);	
}*/
