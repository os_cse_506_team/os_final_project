#ifndef _STRUCT_UTILITY_H
#define _ELF64_H

#define INITIAL_STACK_SIZE 512

struct node
{
        struct node* next;
        struct node* previous;
        uint64_t address;
        uint64_t used;
};

struct vpblock {
	uint64_t p_start;
	uint64_t v_start;
};

struct task_struct {
	uint64_t kstack[INITIAL_STACK_SIZE];
	uint64_t pid;
	uint64_t rsp;
	enum {RUNNING, SLEEPING, ZOMBIE} state;
	int exit_status;
};

#endif
